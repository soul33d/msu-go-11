package main

import (
	"sort"
	"strconv"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(s []int) (result string) {
	for _, e := range s {
		result += strconv.Itoa(e)
	}
	return result
}

func MergeSlices(f []float32, i []int32) []int {
	result := make([]int, 0, len(f)+len(i))
	for _, e := range f {
		result = append(result, int(e))
	}
	for _, e := range i {
		result = append(result, int(e))
	}
	return result
}

func GetMapValuesSortedByKey(m map[int]string) []string {
	keys := make([]int, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	result := make([]string, 0, len(m))
	for _, e := range keys {
		result = append(result, m[e])
	}
	return result
}
