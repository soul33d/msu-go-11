package main

import (
	"fmt"
	"strconv"
	"time"
)

type Calendar struct {
	time.Time
}

func NewCalendar(t time.Time) (result Calendar) {
	return Calendar{t}
}

func (c Calendar) CurrentQuarter() int {
	i, err := strconv.ParseInt(c.Format("1"), 10, 8)
	if err != nil {
		panic(err)
	}
	switch int(i) {
	case 1, 2, 3:
		return 1
	case 4, 5, 6:
		return 2
	case 7, 8, 9:
		return 3
	case 10, 11, 12:
		return 4
	}
	return -1
}

func main() {
	parse, _ := time.Parse("2006-01-02", fmt.Sprintf("2015-%s-15", "12"))
	NewCalendar(parse)
	fmt.Println(parse)
}
